﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.Diagnostics;


namespace CardsWar___client
{
    public partial class Form1 : Form
    {
        System.Windows.Forms.PictureBox[] allImages = new System.Windows.Forms.PictureBox[10];
        NetworkStream clientStream;
        bool playerPickCard = false;
        string pickedCard = "";

        public Form1()
        {
            InitializeComponent();
            Thread openClientThread= new Thread(OpenClientSocket);
            openClientThread.Start();
        }

        public void OpenClientSocket(object obj)
        {
            try
            {
                TcpClient client = new TcpClient();
                IPEndPoint serverEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 8820);
                client.Connect(serverEndPoint);
                this.clientStream = client.GetStream();
                Thread handleWithServerThread = new Thread(HandleWithServer);
                handleWithServerThread.Start();
            }
            catch (Exception e) { Console.WriteLine(e); }

        }

        public void CloseAllCards()
        {
            for(int i = 0; i<10;i++)
            {
                this.allImages[i].Image = global::CardsWar___client.Properties.Resources.card_back_red;
            }
        }

        public void HandleWithServer(object obj)
        {
            byte[] buffer = new byte[4096];
            string msg = "";
            bool keepListening = true;
            while (keepListening)
            { 
                int bytesRead = clientStream.Read(buffer, 0, 4096);                              
                msg = new ASCIIEncoding().GetString(buffer, 0, bytesRead);
                if (msg[0] == '0')
                {
                    GenerateCards();
                    //Thread generateCardsThread = new Thread(GenerateCards);
                    //generateCardsThread.Start();
                }
                else if (msg[0] == '1')
                {
                    compareOpponensCard(msg);
                }
                else if (msg[0] == '2')
                {
                    clientStream.Close();
                    keepListening = false;
                    this.Close();
                }
            }
        }

        public void compareOpponensCard(string c)
        {
            while (this.playerPickCard == false) { Thread.Sleep(1); }
            SetOpponentsCard(c);
            //System.Threading.EventWaitHandle waitHandle = new System.Threading.AutoResetEvent(this.playerPickCard == true);
            if(int.Parse(this.pickedCard.Substring(1,2))==1 && int.Parse(c.Substring(1, 2)) == 1) { }//nothing, its a tay
            else if(int.Parse(this.pickedCard.Substring(1, 2)) == 1 && int.Parse(c.Substring(1, 2)) != 1)
            {
                MyWin();
            }
            else if(int.Parse(this.pickedCard.Substring(1, 2)) != 1 && int.Parse(c.Substring(1, 2)) == 1)
            {
                MyLost();
            }
            else if(int.Parse(this.pickedCard.Substring(1, 2))> int.Parse(c.Substring(1, 2)))
            {
                MyWin();
            }
            else if(int.Parse(this.pickedCard.Substring(1, 2)) == int.Parse(c.Substring(1, 2)))
            {
                //nothing
            }
            else
            {
                MyLost();
            }
            
            
        }

        public void SetOpponentsCard(string s)
        {
            string fileName = "";
            if(s[1]=='0'&&s[2]=='1')
            {
                fileName += "ace";
                fileName += "_of_";
                if (s[3] == 'H')
                {
                    fileName += "hearts.png";
                }
                else if (s[3] == 'C')
                {
                    fileName += "clubs.png";
                }
                else if (s[3] == 'S')
                {
                    fileName += "spades.png";
                }
                else if (s[3] == 'D')
                {
                    fileName += "diamonds.png";
                }
            }
            else
            {
                if (int.Parse(s.Substring(1, 2)) > 1 && int.Parse(s.Substring(1, 2)) < 11)
                {
                    fileName += int.Parse(s.Substring(1, 2)).ToString();
                    fileName += "_of_";
                    if (s[3] == 'H')
                    {
                        fileName += "hearts.png";
                    }
                    else if (s[3] == 'C')
                    {
                        fileName += "clubs.png";
                    }
                    else if (s[3] == 'S')
                    {
                        fileName += "spades.png";
                    }
                    else if (s[3] == 'D')
                    {
                        fileName += "diamonds.png";
                    }
                }
                else
                { 
                    if (s.Substring(1, 2) == "11")//jack
                    {
                        fileName += "jack";
                    }
                    else if (s.Substring(1, 2) == "12")//queen
                    {
                        fileName += "queen";
                    }
                    else if (s.Substring(1, 2) == "13")//king
                    {
                        fileName += "queen";
                    }
                    fileName += "_of_";
                    if (s[3] == 'H')
                    {
                        fileName += "hearts2.png";
                    }
                    else if (s[3] == 'C')
                    {
                        fileName += "clubs2.png";
                    }
                    else if (s[3] == 'S')
                    {
                        fileName += "spades2.png";
                    }
                    else if (s[3] == 'D')
                    {
                        fileName += "diamonds2.png";
                    }
                }
            }
            Invoke((MethodInvoker)delegate {
                string RunningPath = AppDomain.CurrentDomain.BaseDirectory;
                DirectoryInfo d = new DirectoryInfo(Path.GetFullPath(Path.Combine(RunningPath, @"..\..\")) + "Resources");
                anotherPlayerPicBox.BackgroundImage = Image.FromFile(d.ToString() + @"\" + fileName);
                //anotherPlayerPicBox.Image = Image.FromFile(d.ToString() + @"\" + fileName);
            });

        }


        public void MyWin()
        {
            Invoke((MethodInvoker)delegate {
                label1.Text = "Youre Score = " + (int.Parse(label1.Text.Split('=')[1]) + 1).ToString();
                label2.Text = "opponen't Score = " + (int.Parse(label2.Text.Split('=')[1]) - 1).ToString();

            });
        }

        public void MyLost()
        {
            Invoke((MethodInvoker)delegate {
                label1.Text = "Youre Score = " + (int.Parse(label1.Text.Split('=')[1]) - 1).ToString();
                label2.Text = "opponen't Score = " + (int.Parse(label2.Text.Split('=')[1]) + 1).ToString();
            });
        }

        public void CardsEnOrDe(bool b)
        {
            Invoke((MethodInvoker)delegate {
                for (int i = 0; i < 10; i++)
                {
                    //this.Controls[i].Enabled = b;
                    allImages[i].Enabled = b;
                }
            });
        }

        


        








        public void GenerateCards()
        {
            Point cardLocation = new Point(10, 320);

            for (int i = 0; i < 10; i++)
            {
                System.Windows.Forms.PictureBox curPic = new PictureBox();
                curPic.Name = "dynamicImage" + i;
                curPic.Location = cardLocation;
                curPic.Size = new System.Drawing.Size(100, 114);
                curPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
                //curPic.ImageLocation = (@"C:\Users\magshimim\Documents\magshimim\Classes\11grade\C++\L20\DanielHimsani-HW21\Project - DanielHimsaniHW20\CardsWar - client\Resources\3_of_clubs.png");
                curPic.Image = global::CardsWar___client.Properties.Resources.card_back_red;

                curPic.Click += delegate (object sender1, EventArgs e1)
                {
                    string RunningPath = AppDomain.CurrentDomain.BaseDirectory;
                    DirectoryInfo d = new DirectoryInfo(Path.GetFullPath(Path.Combine(RunningPath, @"..\..\")) + "Resources");
                    FileInfo[] Files = d.GetFiles("*.png");
                    Random rnd = new Random();
                    int rndNum = rnd.Next(0, 52);
                    bool flag = true;
                    while (flag)
                    {
                        if (!Files[rndNum].Name.StartsWith("card")) flag = false;
                        else rndNum = rnd.Next(0, 52);
                    }

                    string msg = "1";
                    if (Files[rndNum].Name.Split('_')[0][0]>='1' && Files[rndNum].Name.Split('_')[0][0] <= '9')
                    {
                        if (int.Parse(Files[rndNum].Name.Split('_')[0]) < 10)
                        {
                            msg += "0" + int.Parse(Files[rndNum].Name.Split('_')[0].ToString());
                        }
                        else msg += int.Parse(Files[rndNum].Name.Split('_')[0].ToString());
                    }
                    else
                    {
                        if (Files[rndNum].Name.Split('_')[0].ToString() == "jack")
                        {
                            msg += "11";
                        }
                        else if (Files[rndNum].Name.Split('_')[0].ToString() == "king")
                        {
                            msg += "13";
                        }
                        else if (Files[rndNum].Name.Split('_')[0].ToString() == "queen")
                        {
                            msg += "12";
                        }
                        else if (Files[rndNum].Name.Split('_')[0].ToString() == "ace")
                        {
                            msg += "01";
                        }
                    }

                    if (Files[rndNum].Name.Split('_')[2].StartsWith("spades"))
                    {
                        msg += "S";
                    }
                    else if (Files[rndNum].Name.Split('_')[2].StartsWith("hearts"))
                    {
                        msg += "H";
                    }
                    else if (Files[rndNum].Name.Split('_')[2].StartsWith("diamonds"))
                    {
                        msg += "D";
                    }
                    else if (Files[rndNum].Name.Split('_')[2].StartsWith("clubs"))
                    {
                        msg += "C";
                    }

                    byte[] buffer = new ASCIIEncoding().GetBytes(msg);
                    clientStream.Write(buffer, 0, buffer.Length);
                    clientStream.Flush();
                    CloseAllCards();
                    ((PictureBox)sender1).ImageLocation = d.ToString() + @"\" + Files[rndNum].Name;
                    this.playerPickCard = true;
                    this.pickedCard = msg;
                    //((PictureBox)sender1).ImageLocation = Path.Combine(RunningPath, @"..\..\"))+"Resources"+@"\"+Files[rndNum].Name;
                };
                

            Invoke((MethodInvoker)delegate { this.Controls.Add(curPic); });

                //this.Controls.Add(curPic);
                this.allImages[i] = curPic;
                
                cardLocation.X += curPic.Size.Width + 10;

                
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            clientStream.Close();
            MessageBox.Show("Daniel Himsani");
            this.Close();
        }
    }
}
